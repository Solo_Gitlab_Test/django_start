from django.shortcuts import render
from django.http.response import HttpResponse
from . import models


# Create your views here.
def index(request):
    category_list = models.Category.objects.all()
    context = {'category_list': category_list}
    return render(request, 'index.html', context)
