from django.contrib import admin
from . import models


class CategoryAdmin(admin.ModelAdmin):
    fields = ['id', 'name', 'description', 'created_at', 'updated_at']
    readonly_fields = ['id', 'created_at', 'updated_at']


admin.site.register(models.Category, CategoryAdmin)